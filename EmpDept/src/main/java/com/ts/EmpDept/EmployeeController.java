//package com.ts.EmpDept;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//
//import com.dao.EmployeeDao;
//import com.model.Employee;
//
//@RestController
//@RequestMapping("/employees")
//public class EmployeeController {
//
//    @Autowired
//    private EmployeeDao employeeDao;
//
//    @GetMapping("getEmployees")
//    public List<Employee> getEmployees() {
//        return employeeDao.getEmployees();
//    }
//
//	
//	@GetMapping("getEmployeeById/{employeeId}")
//	public Employee getEmployeeById(@PathVariable int employeeId) {
//		return employeeDao.getEmployeeById(employeeId);
//	}
//	
//	@GetMapping("getEmployeeByName/{employeeName}")
//	public Employee getEmployeeByName(@PathVariable String employeeName) {
//		return employeeDao.getEmployeeByName(employeeName);
//	}
//	
//	@GetMapping("empLogin/{emailId}/{password}")
//	public Employee employeeLogin(@PathVariable String emailId, @PathVariable String password) {
//		return employeeDao.employeeLogin(emailId, password);
//	}
//	
//	@PostMapping("addEmployee")
//	public Employee addEmployee(@RequestBody Employee employee) {
//		
//      	return employeeDao.addEmployee(employee);}
//
//	
//	@PutMapping("updateEmployee")
//	public Employee updateEmployee(@RequestBody Employee employee) {
//		return employeeDao.updateEmployee(employee);
//	}
//	
//	@DeleteMapping("deleteEmployeeById/{employeeId}")
//	public String deleteEmployeeById(@PathVariable int employeeId) {
//		employeeDao.deleteEmployeeById(employeeId);
//		return "Employee with empId:" + employeeId + " Deleted Successfully!!!";
//	}
//	@GetMapping("/generateOTP/{phoneNumber}")
//    public String generateOTP(@PathVariable String phoneNumber) {
//        // Generate OTP and send it to the provided phone number (you may use an SMS service for this)
//        String otp = employeeDao.generateOTP();
//
//        // TODO: Implement code to send OTP to the mobile number (use a third-party SMS service or any preferred method)
//
//        return "OTP generated and sent to " + phoneNumber;
//    }
//}
package com.ts.EmpDept;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.dao.DepartmentDao;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeDao empDao;

	@Autowired
	DepartmentDao deptDao;

	@GetMapping("getempLogin/{emailId}/{password}")
	public Employee empLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		return empDao.employeeLoginByEmail(emailId, password);
	}

	@GetMapping("getempLogin/phoneNumber")
	public Employee empLogin1(@PathVariable("credential") String credential,
			@PathVariable("password") String password) {
		if (credential.contains("@")) {
			return empDao.employeeLoginByEmail(credential, password);
		} else {
			return empDao.empLoginByPhoneNumber(credential, password);
		}
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("getEmployees")
	public List<Employee> getEmployees() {
		return empDao.getEmployees();
	}

	@GetMapping("getEmployeeById/{empId}")
	public Employee getEmployeeById(@PathVariable("empId") int empId) {
		return empDao.getEmployeeById(empId);
	}

	@GetMapping("getEmployeeByName/{empName}")
	public Employee getEmployeeByName(@PathVariable("empName") String empName) {
		return empDao.getEmployeeByName(empName);
	}

	@PostMapping("addEmployee")
	public Employee addEmployee(@RequestBody Employee emp) {
		return empDao.addEmployee(emp);
	}

	@PutMapping("updateEmployee")
	public Employee updateEmployee(@RequestBody Employee emp) {
		return empDao.updateEmployee(emp);
	}

	@DeleteMapping("deleteEmployeeById/{empId}")
	public String deleteEmployeeById(@PathVariable("empId") int empId) {
		empDao.deleteEmployeeById(empId);
		return "Employee with empId: " + empId + ", Deleted Successfully";
	}

}
