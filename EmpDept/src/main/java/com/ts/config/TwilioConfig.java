package com.ts.config;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class TwilioConfig {

    @Value("${twilio.account.sid}")
    private String accountSid;

    @Value("${twilio.auth.token}")
    private String authToken;

    @Value("${twilio.phone.number}")
    private String twilioPhoneNumber;
    @Bean
    public void initTwilio() {
        Twilio.init(accountSid, authToken);
    }

    public void sendOtp(String toPhoneNumber, String otp) {
       

        Message message = Message.creator(
        		 
                new PhoneNumber(toPhoneNumber),  // to
                new PhoneNumber(twilioPhoneNumber),  // from
                "Your OTP is: " + otp)
                .create();

        System.out.println("Twilio Message SID: " + message.getSid());
    }
}