//////package com.dao;
//////
//////import org.springframework.data.jpa.repository.JpaRepository;
//////import org.springframework.data.jpa.repository.Query;
//////import org.springframework.data.repository.query.Param;
//////import org.springframework.stereotype.Repository;
//////
//////import com.model.Employee;
//////
//////@Repository
//////public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
//////
//////	@Query("from Employee where empName = :employeeName")
//////	Employee findByName(@Param("employeeName") String employeeName);
//////
//////	@Query("from Employee where emailId = :emailId and password = :password")
//////	Employee employeeLogin(@Param("emailId") String emailId, @Param("password") String password);
//////
//////}
////
////package com.dao;
////
////import org.springframework.data.jpa.repository.JpaRepository;
////import org.springframework.data.jpa.repository.Query;
////import org.springframework.data.repository.query.Param;
////import org.springframework.stereotype.Repository;
////
////import com.model.Employee;
////
////@Repository
////public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
////
////	@Query("from Employee where empName = :eName")
////	Employee findbyName(@Param("eName") String empName);
////
////	@Query("from Employee e where e.emailId = :emailId and e.password = :password")
////	Employee empLogin(@Param("emailId") String emailId, @Param("password") String password);
////	
////	@Query("from Employee e where e.emailId = :emailId")
////	Employee findByEmailId(@Param("emailId") String emailId);
////
////	@Query("from Employee e where e.phoneNumber = :phoneNumber")
////	Employee findByPhoneNumber(@Param("phoneNumber") String phoneNumber);
////
////
////}
//package com.dao;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import com.model.Employee;
//
//@Repository
//public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
//
//	@Query("from Employee where empName = :eName")
//	Employee findbyName(@Param("eName") String empName);
//
//	@Query("from Employee e where e.emailId = :emailId and e.password = :password")
//	Employee empLogin(@Param("emailId") String emailId, @Param("password") String password);
//	
//	@Query("from Employee e where e.emailId = :emailId")
//	Employee findByEmailId(@Param("emailId") String emailId);
//
//	@Query("from Employee e where e.phoneNumber = :phoneNumber")
//	Employee findByPhoneNumber(@Param("phoneNumber") String phoneNumber);
//
//
//}

package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	@Query("from Employee where empName = :eName")
	Employee findbyName(@Param("eName") String empName);

	@Query("from Employee e where e.emailId = :emailId and e.password = :password")
	Employee empLogin(@Param("emailId") String emailId, @Param("password") String password);
	
	@Query("from Employee e where e.emailId = :emailId")
	Employee findByEmailId(@Param("emailId") String emailId);

	@Query("from Employee e where e.phoneNumber = :phoneNumber")
	Employee findByPhoneNumber(@Param("phoneNumber") String phoneNumber);


}